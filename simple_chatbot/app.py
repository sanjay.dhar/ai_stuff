import os
import sys
import json
import logging
import requests
import oauthlib
import streamlit as st
from oauthlib.oauth2 import BackendApplicationClient
from requests.auth import AuthBase, HTTPBasicAuth
from requests_oauthlib import OAuth2Session
from fastapi import HTTPException
from gcp_secrets import access_secret_version

token_url = "https://api.veolia.com/security/v2/oauth/token"
gcp_project_id = "us-ist-vna-ctosbx-dev"

client_id = access_secret_version(gcp_project_id, "client_id", "latest")
client_secret = access_secret_version(gcp_project_id, "client_secret", "latest")
client_email = access_secret_version(gcp_project_id, "client_email", "latest")

# Function to get environment variable
def get_env(key, default=None):
    value = os.environ.get(key, default)
    if value == 'True' or value == 'true':
        value = True
    elif value == 'False' or value == 'false':
        value = False
    return value



#client_id = (get_env("CLIENT_ID"))
#client_secret = (get_env("CLIENT_SECRET"))
#client_email = (get_env("CLIENT_EMAIL"))

auth = HTTPBasicAuth(client_id, client_secret)
client = BackendApplicationClient(client_id=client_id)
oauth = OAuth2Session(client=client)

# Function to get the access token
def get_access_token():
    try:
        token = oauth.fetch_token(token_url=token_url, client_id=client_id, client_secret=client_secret)
        return token['access_token']
    except oauthlib.oauth2.TokenError as e:
        logging.error(f"Error fetching access token: {e}")
        raise HTTPException(status_code=500, detail="Error fetching access token")
    except Exception as e:
        logging.error(f"Error fetching access token: {e}")
        raise HTTPException(status_code=500, detail="Error fetching access token")
    return None

access_token = get_access_token()

def chat_with_api(prompt):
    api_url = 'https://api.veolia.com/llm/veoliasecuregpt/v1/answer'
    # API endpoint and headers
    api_url = f"{api_url}"


    headers = {
            "Authorization": f"Bearer {access_token}",
            "Content-Type": "application/json"
    }

    data = {
    "useremail": f"{client_email}",

    "history": [
        {
            "role": "user",
            "content": prompt
        }
    ],
    "temperature": 0.1,
    "top_p": 1,
    "model": "anthropic.claude-v3",
    #"model": "gpt-4",
}
    #response = requests.post(api_url, headers=headers, json=data)
    response = requests.post(api_url, headers=headers, data=json.dumps(data))

    if response.status_code == 200:
        return (response.json())
        #return json.dumps(response)
    else:
        return "Sorry, there was an error communicating with the API."
        #return (response.text)


st.title("Simple VNA chat app")

# Initialize chat history
if "messages" not in st.session_state:
    st.session_state.messages = []

# Display chat messages from history on app rerun
for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])


# Accept user input
if prompt := st.chat_input("Ask or tell me to do something"):
    # Add user message to chat history
    st.session_state.messages.append({"role": "user", "content": prompt})
    # Display user message in chat message container
    with st.chat_message("user"):
        st.markdown(prompt)

    # Display assistant response in chat message container
    response = chat_with_api(prompt)
    st.session_state.messages.append({"role": "assistant", "content": response})
    with st.chat_message("assistant"):
        response = st.write(response)
