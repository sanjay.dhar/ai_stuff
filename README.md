# Basic Gen AI apps

You will find two basic Gen AI apps here

1. In the ```cli_chatbot``` folder you will find code you can run in python which will let you enter a prompt on the command line and the output will be an answer from the LLM
2. In the ```simple_chatbot``` folder, you will find a Dockerized app based on Streamlit that will give you a web based app that you can use to chat with an LLM
