# Simple VNA Chatbot

This is a basic python CLI based app that is a simple chatbot that talks to the Veolia SecureGPT API. You will need a ```Client ID``` and ```Client Secret```  before you can deploy this app. You can go to the [Veolia Developer Portal](https://developers.api.veolia.com/apps) and create an app. You will need to select ```prd-vesa-veoliasecuregpt``` as the product when you create your app. 

This is a very basic app but is secure since it does not store any secrets in code. This app uses [environment variables](https://www.twilio.com/en-us/blog/environment-variables-python) to store sensitive informartion. 

There is lots of room to improve this app, so feel free to have at it.