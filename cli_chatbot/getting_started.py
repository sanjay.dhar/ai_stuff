import os
import sys
import json
import logging
import requests
import oauthlib
from oauthlib.oauth2 import BackendApplicationClient
from requests.auth import AuthBase, HTTPBasicAuth
from requests_oauthlib import OAuth2Session
from fastapi import HTTPException

# Function to get environment variable
def get_env(key, default=None):
    value = os.environ.get(key, default)
    if value == 'True' or value == 'true':
        value = True
    elif value == 'False' or value == 'false':
        value = False
    return value

token_url = "https://api.veolia.com/security/v2/oauth/token"
api_url = 'https://api.veolia.com/llm/veoliasecuregpt/v1/answer'

client_id = (get_env("CLIENT_ID"))
client_secret = (get_env("CLIENT_SECRET"))
client_email = (get_env("CLIENT_EMAIL"))

auth = HTTPBasicAuth(client_id, client_secret)
client = BackendApplicationClient(client_id=client_id)
oauth = OAuth2Session(client=client)

# Function to get the access token
def get_access_token():
    try:
        token = oauth.fetch_token(token_url=token_url, client_id=client_id, client_secret=client_secret)
        return token['access_token']
    except oauthlib.oauth2.TokenError as e:
        logging.error(f"Error fetching access token: {e}")
        raise HTTPException(status_code=500, detail="Error fetching access token")
    except Exception as e:
        logging.error(f"Error fetching access token: {e}")
        raise HTTPException(status_code=500, detail="Error fetching access token")
    return None

access_token = get_access_token()

headers = {
          "Authorization": f"Bearer {access_token}",
          "Content-Type": "application/json"
  }

while True: 
    try:
        prompt = input("Give me a task or ask me a question (Ctrl-C to quit): ")

        data = {
            "useremail": f"{client_email}",

            "history": [
                {
                    "role": "user",
                    "content": f"{prompt}"
                }
            ],
            "temperature": 0.1,
            "top_p": 1,
            "model": "anthropic.claude-v3",
            #"model": "gpt-4",
        }
        response = requests.post(api_url, headers=headers, data=json.dumps(data))
        print(response.text)
    except KeyboardInterrupt:
        print("\nExiting...")
        sys.exit(0)   